public class CasePaireOuImpaire extends Case {

    public CasePaireOuImpaire(int position) {
        super(position);
    }

    @Override
    public int nouvellePosition(int valeurDe) {
        return this.getPosition() + (valeurDe % 2 == 0 ? valeurDe : -valeurDe);
    }
}
