public class CaseAttente extends Case {

    private int valeurDe;

    public CaseAttente(int position, int valeurDe) {
        super(position);
        this.valeurDe = valeurDe;
    }

    @Override
    public int nouvellePosition(int valeurDe) {
        return this.getPosition() + (this.valeurDe == valeurDe ? valeurDe : 0);
    }
}
