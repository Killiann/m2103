public class TestPlateauPosition {

    private final int position;
    private final int valeurDe;
    private final int nouvellePosition;

    public TestPlateauPosition(int position, int valeurDe, int nouvellePosition) {
        this.position = position;
        this.valeurDe = valeurDe;
        this.nouvellePosition = nouvellePosition;
    }

    public int getPosition() {
        return position;
    }

    public int getValeurDe() {
        return valeurDe;
    }

    public int getNouvellePosition() {
        return nouvellePosition;
    }

    @Override
    public String toString() {
        return "TestPlateauPosition{" +
                "position=" + position +
                ", valeurDe=" + valeurDe +
                ", nouvellePosition=" + nouvellePosition +
                '}';
    }
}
