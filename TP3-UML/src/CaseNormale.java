public class CaseNormale extends Case {

    public CaseNormale(int position) {
        super(position);
    }

    @Override
    public int nouvellePosition(int valeurDe) {
        return getPosition()+valeurDe;
    }
}
