public class Plateau {

    private static int NB_CASES = 49;

    private Case[] cases = new Case[NB_CASES];

    public Plateau() {
        cases[5] = new CaseSaut(5, 6, 20);
        cases[12] = new CaseAttente(12, 2);
        cases[15] = new CasePaireOuImpaire(15);
        cases[16] = new CaseSaut(16, 4, 22);
        cases[18] = new CaseAttente(18, 5);
        cases[20] = new CasePaireOuImpaire(20);
        cases[24] = new CaseAttente(24, 2);
        cases[27] = new CaseAttente(27, 5);
        cases[33] = new CaseAttente(33, 4);
        cases[36] = new CaseSaut(36, 1, 10);
        cases[45] = new CaseSaut(45, 2, 0);

        for (int i = 0; i < NB_CASES; i++) {
            if(cases[i] == null) cases[i] = new CaseNormale(i);
        }
    }

    public int nouvellePosition(int position, int valeurDe) {
        Case casee = cases[position];
        int i = casee.nouvellePosition(valeurDe);
        if(i > 48) {
            i = 48 -  nouvellePositionQuandDépassement(i);
        }
        return i;
    }

    public int nouvellePositionQuandDépassement(int nouvellePosition) {
        return nouvellePosition - (NB_CASES-1);
    }
}
