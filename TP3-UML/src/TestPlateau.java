import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestPlateau {

    private Plateau plateau = new Plateau();

    private List<TestPlateauPosition> positions = Arrays.asList(
            new TestPlateauPosition(0, 1, 1),
            new TestPlateauPosition(1, 3, 4),
            new TestPlateauPosition(5, 6, 20),
            new TestPlateauPosition(5, 3, 8),
            new TestPlateauPosition(12, 2, 14),
            new TestPlateauPosition(12, 3, 12),
            new TestPlateauPosition(15, 4, 19),
            new TestPlateauPosition(15, 5, 10),
            new TestPlateauPosition(16, 4, 22),
            new TestPlateauPosition(16, 1, 17),
            new TestPlateauPosition(18, 5, 23),
            new TestPlateauPosition(18, 6, 18),
            new TestPlateauPosition(20, 2, 22),
            new TestPlateauPosition(20, 3, 17),
            new TestPlateauPosition(24, 2, 26),
            new TestPlateauPosition(24, 6, 24),
            new TestPlateauPosition(27, 5, 32),
            new TestPlateauPosition(27, 4, 27),
            new TestPlateauPosition(33, 4, 37),
            new TestPlateauPosition(33, 2, 33),
            new TestPlateauPosition(36, 1, 10),
            new TestPlateauPosition(36, 6, 42),
            new TestPlateauPosition(45, 2, 0),
            new TestPlateauPosition(45, 3, 48),
            new TestPlateauPosition(45, 4, 47),
            new TestPlateauPosition(45, 5, 46),
            new TestPlateauPosition(45, 6, 45),
            new TestPlateauPosition(47, 6, 43)
    );

    @Test
    public void testPlateau() {
        positions.forEach(testPlateauPosition -> {
            System.out.println("Test position " + testPlateauPosition.getPosition());
            assertEquals(testPlateauPosition.getNouvellePosition(), plateau.nouvellePosition(testPlateauPosition.getPosition(), testPlateauPosition.getValeurDe()));
        });
    }
}
