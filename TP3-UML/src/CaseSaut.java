public class CaseSaut extends Case {

    private int valeurDe, caseArrive;

    public CaseSaut(int position, int valeurDe, int caseArrive) {
        super(position);
        this.valeurDe = valeurDe;
        this.caseArrive = caseArrive;
    }

    @Override
    public int nouvellePosition(int valeurDe) {
        return this.valeurDe == valeurDe ? caseArrive : this.getPosition() + valeurDe;
    }
}
