package basic;

import java.util.Scanner;

public class ResolutionEquation {

    public static void main(String[] args) {
        new ResolutionEquation();
    }

    public ResolutionEquation() {
        int a = 0, b = 0, c = 0;

        Scanner scanner = new Scanner(System.in);

        a = getValue('a', scanner, true);
        b = getValue('b', scanner, false);
        c = getValue('c', scanner, false);

        System.out.println("Vous venez d'entrer l'équation: " + (a+"x²+"+b+"x+"+c));

        int dis = (b*b)-(4*a*c);
        System.out.println("Discriminant: " + dis);
        if(dis < 0) {
            System.out.println("Aucune solutions");
        } else if(dis > 0) {
            int x1 = (int) ((-b+Math.sqrt(dis))/(2*a));
            int x2 = (int) ((-b-Math.sqrt(dis))/(2*a));

            System.out.println("2 solutions: x1=" + x1 + ", x2=" + x2);
        } else {
            int x = -b/2*a;
            System.out.println("Une solution: x="+x);
        }
    }

    private int getValue(char c, Scanner scanner, boolean block0) {
        System.out.println("Valeur de "+c+":");
        int i = 0;
        boolean next = false;
        do {
            try {
                i = scanner.nextInt();
                next = !block0 || i != 0;
            } catch (Exception e) {
                System.out.println("Erreur...");
                scanner.next();
            }
        } while (!next);

        return i;
    }
}
