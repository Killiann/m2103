package basic;

import java.util.Scanner;

public class Asterisques {

    public static void main(String[] args) {
        new Asterisques();
    }

    public Asterisques() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entrez n:");
        int n = scanner.nextInt();

        for (int i = 0; i<n; i++) {
            for (int u = 0; u<=i; u++) {
                System.out.print('*');
            }
            System.out.println();
        }

        for (int i = n; i>0; i--) {
            for (int u = 0; u<n; u++) {
                System.out.print(u<(i-1) ? ' ' : '*');
            }
            System.out.println();
        }
    }
}
