package basic;

import java.util.Scanner;

public class MinimumTableau2 {

    public static void main(String[] args) {
        new MinimumTableau2();
    }

    int[] tab = new int[10];
    public MinimumTableau2() {
        System.out.println("Saisir les valeurs entières du tableau :");
        Scanner scanner = new Scanner(System.in);
        for(int i = 0; i < tab.length; i++) {
            tab[i] = scanner.nextInt();
        }

        int i = Integer.MAX_VALUE;
        for (int v : tab) {
            if(v < i) i = v;
        }

        System.out.println("Valeur minimale du tableau :" + i);
    }
}
