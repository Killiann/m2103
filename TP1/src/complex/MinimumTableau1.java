package complex;

import java.util.Arrays;

public class MinimumTableau1 {

    public static void main(String[] args) {
        new MinimumTableau1();
    }

    int[] tab = {55, -67, 112, 6, -210, 5, 690, 0, 34, 88};
    public MinimumTableau1() {
        System.out.println("Valeur minimale du tableau :" + (Arrays.stream(tab).min().getAsInt()));
    }
}
