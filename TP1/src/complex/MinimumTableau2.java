package complex;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.IntStream;

public class MinimumTableau2 {

    public static void main(String[] args) {
        new MinimumTableau2();
    }

    int[] tab = new int[10];
    public MinimumTableau2() {
        System.out.println("Saisir les valeurs entières du tableau :");
        Scanner scanner = new Scanner(System.in);
        IntStream.range(0, tab.length).forEach(value -> tab[value] = scanner.nextInt());
        System.out.println("Valeur minimale du tableau :" + (Arrays.stream(tab).min().getAsInt()));
    }
}
