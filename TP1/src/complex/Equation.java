package complex;

import java.util.Scanner;

public class Equation {

    private final int a;
    private final int b;
    private final int c;

    public Equation(Scanner scanner) {
        a = getValue('a', scanner, true);
        b = getValue('b', scanner, false);
        c = getValue('c', scanner, false);
    }

    public int getDiscriminant() {
        return (b*b)-(4*a*c);
    }

    public int[] getValues() {
        int[] tab;
        int dis = getDiscriminant();
        if(dis > 0) {
            tab = new int[2];
            tab[0] = (int) ((-b+Math.sqrt(dis))/(2*a));
            tab[1] = (int) ((-b-Math.sqrt(dis))/(2*a));
        } else {
            tab = new int[1];
            tab[0] = -b/2*a;
        }

        return tab;
    }

    private int getValue(char c, Scanner scanner, boolean block0) {
        System.out.println("Valeur de "+c+":");
        int i = 0;
        boolean next = false;
        do {
            try {
                i = scanner.nextInt();
                next = !block0 || i != 0;
            } catch (Exception e) {
                System.out.println("Erreur...");
                scanner.next();
            }
        } while (!next);

        return i;
    }

    @Override
    public String toString() {
        return (a+"x²+"+b+"x+"+c);
    }
}
