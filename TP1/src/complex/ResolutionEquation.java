package complex;

import java.util.Scanner;

public class ResolutionEquation {

    public static void main(String[] args) {
        new ResolutionEquation();
    }

    public ResolutionEquation() {
        Scanner scanner = new Scanner(System.in);
        Equation equation = new Equation(scanner);
        System.out.println("Vous venez d'entrer l'équation: " + equation.toString());

        System.out.println("Discriminant: " + equation.getDiscriminant());
        int[] vals = equation.getValues();
        if(vals.length == 0) {
            System.out.println("Aucune solutions");
        } else if(vals.length == 2) {
            System.out.println("2 solutions: x1=" + vals[0] + ", x2=" + vals[1]);
        } else {
            System.out.println("Une solution: x="+vals[0]);
        }
    }
}
