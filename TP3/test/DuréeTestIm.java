import basic.DuréeIm;
import org.junit.Test;

import static org.junit.Assert.*;

public class DuréeTestIm {

    @Test
    public void testGetters() {
        DuréeIm d = new DuréeIm(1, 2, 3);
        assertEquals(1, d.getHeures());
        assertEquals(2, d.getMinutes());
        assertEquals(3, d.getSecondes());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testHeuresNegative() {
        new DuréeIm(-1, 2, 3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMinutesNegative() {
        new DuréeIm(1, -2, 3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMinutesSuperieur59() {
        new DuréeIm(1, 60, 3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSecondesNegative() {
        new DuréeIm(1, 2, -3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSecondesSuperieur59() {
        new DuréeIm(1, 2, 60);
    }

    @Test
    public void testEgal() {
        assertTrue(new DuréeIm(1, 2, 3).égal(new DuréeIm(1, 2, 3)));
    }

    @Test
    public void testNonEgal() {
        assertFalse(new DuréeIm(1, 2, 3).égal(new DuréeIm(2, 2, 3)));
        assertFalse(new DuréeIm(1, 2, 3).égal(new DuréeIm(1, 1, 3)));
        assertFalse(new DuréeIm(1, 2, 3).égal(new DuréeIm(1, 2, 2)));
    }

    @Test
    public void testAjouterUneSeconde() {
        DuréeIm d123 = new DuréeIm(1, 2, 3);
        d123=d123.ajouterUneSeconde();
        assertTrue(new DuréeIm(1, 2, 4).égal(d123));
        DuréeIm d1259 = new DuréeIm(1, 2, 59);
        d1259=d1259.ajouterUneSeconde();
        assertTrue(new DuréeIm(1, 3, 0).égal(d1259));
        DuréeIm d5959 = new DuréeIm(1, 59, 59);
        d5959=d5959.ajouterUneSeconde();
        assertTrue(new DuréeIm(2, 0, 0).égal(d5959));
    }

    @Test
    public void testInf() {
        assertTrue(new DuréeIm(1, 2, 3).inf(new DuréeIm(2, 2, 3)));
        assertTrue(new DuréeIm(1, 2, 3).inf(new DuréeIm(1, 3, 3)));
        assertTrue(new DuréeIm(1, 2, 3).inf(new DuréeIm(1, 2, 4)));
    }

    @Test
    public void testNonInf() {
        assertFalse(new DuréeIm(1, 2, 3).inf(new DuréeIm(1, 2, 3)));
        assertFalse(new DuréeIm(2, 2, 3).inf(new DuréeIm(1, 2, 3)));
        assertFalse(new DuréeIm(1, 3, 3).inf(new DuréeIm(1, 2, 3)));
        assertFalse(new DuréeIm(1, 2, 4).inf(new DuréeIm(1, 2, 3)));
    }

    @Test
    public void testToString() {
        assertEquals("1:2:3", new DuréeIm(1, 2, 3).toString());
    }
}
