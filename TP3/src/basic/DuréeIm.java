package basic;

public final class DuréeIm {

    private final int h, m, s;

    public DuréeIm(int h, int m, int s) {
        if(h < 0 || (m < 0 || m > 59) || (s < 0 || s > 59)) throw new IllegalArgumentException();
        this.h = h;
        this.m = m;
        this.s = s;
    }

    public int getHeures() {
        return h;
    }

    public int getMinutes() {
        return m;
    }

    public int getSecondes() {
        return s;
    }
    
    public boolean égal(DuréeIm durée2) {
        return h == durée2.h && m == durée2.m && s == durée2.s;
    }
    
    public boolean inf(DuréeIm durée2) {
        return h < durée2.h || (h == durée2.h && m < durée2.m) || (h == durée2.h && m == durée2.m && s < durée2.s);
    }
    
    public DuréeIm ajouterUneSeconde() {
        if(s < 59) {
            return new DuréeIm(h, m, s+1);
        } else if(m < 59) {
            return new DuréeIm(h, m+1, 0);
        } else {
            return new DuréeIm(h+1, 0, 0);
        }
    }

    @Override
    public String toString() {
        return h+":"+m+":"+s;
    }
}
