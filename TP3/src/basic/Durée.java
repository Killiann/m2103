package basic;

public class Durée {
    
    private int h, m, s;

    public Durée(int h, int m, int s) {
        if(h < 0 || (m < 0 || m > 59) || (s < 0 || s > 59)) throw new IllegalArgumentException();
        this.h = h;
        this.m = m;
        this.s = s;
    }

    public int getHeures() {
        return h;
    }

    public int getMinutes() {
        return m;
    }

    public int getSecondes() {
        return s;
    }
    
    public boolean égal(Durée durée2) {
        return h == durée2.h && m == durée2.m && s == durée2.s;
    }
    
    public boolean inf(Durée durée2) {
        return h < durée2.h || (h == durée2.h && m < durée2.m) || (h == durée2.h && m == durée2.m && s < durée2.s);
    }
    
    public void ajouterUneSeconde() {
        if(s < 59) {
            s++;
        } else if(m < 59) {
            m++;
            s=0;
        } else {
            h++;
            m=0;
            s=0;
        }
    }

    @Override
    public String toString() {
        return h+":"+m+":"+s;
    }
}
