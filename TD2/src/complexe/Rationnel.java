package complexe;

import exception.RationnelException;

import java.util.stream.LongStream;

public class Rationnel {

    private final long n;
    private final long d;

    public Rationnel(long n, long d) throws RationnelException {
        if(n <= 0 || d == 0) throw new RationnelException();
        this.n = n;
        this.d = d;
    }

    public long getNumérateur() {
        return n;
    }

    public long getDénominateur() {
        return d;
    }

    public Rationnel réduction() throws RationnelException {
        return new Rationnel(n/pgcd(), d/pgcd());
    }

    public Rationnel somme(Rationnel r) throws RationnelException {
        return new Rationnel(n*r.getNumérateur() + r.getNumérateur()*d, d*r.getDénominateur());
    }

    public Rationnel produit(Rationnel r) throws RationnelException {
        return new Rationnel(n*r.getNumérateur(), d*r.getDénominateur());
    }

    public long pgcd() {
        return n > d ? pgcd(n - d, d) : (d > n ? pgcd(n, d - n) : pgcd(n, d));
    }

    private long pgcd(long a, long b) {
        return LongStream.rangeClosed(1, a).filter(i -> i <= a && i <= b && a% i==0 && b%i==0).findFirst().orElse(0);
    }

    @Override
    public String toString() {
        return n+"/"+d;
    }
}
