package basic;

import exception.RationnelException;

public class Rationnel {

    private final long n;
    private final long d;

    public Rationnel(long n, long d) throws RationnelException {
        if(n <= 0 || d == 0) throw new RationnelException();
        this.n = n;
        this.d = d;
    }

    public long getNumérateur() {
        return n;
    }

    public long getDénominateur() {
        return d;
    }

    public Rationnel réduction() throws RationnelException {
        return new Rationnel(n/pgcd(), d/pgcd());
    }

    public Rationnel somme(Rationnel r) throws RationnelException {
        return new Rationnel(n*r.getNumérateur() + r.getNumérateur()*d, d*r.getDénominateur());
    }

    public Rationnel produit(Rationnel r) throws RationnelException {
        return new Rationnel(n*r.getNumérateur(), d*r.getDénominateur());
    }

    public long pgcd() {
        if(n > d) {
            return pgcd(n - d, d);
        } else if(d > n) {
            return pgcd(n, d - n);
        }
        return pgcd(n, d);
    }

    private long pgcd(long a, long b) {
        long pgcd = 0;
        for(int i=1; i <= a && i <= b; i++) {
            if(a% i==0 && b%i==0)
                pgcd = i;
        }
        return pgcd;
    }

    @Override
    public String toString() {
        return n+"/"+d;
    }
}
