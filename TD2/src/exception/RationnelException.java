package exception;

public class RationnelException extends Exception {

    public RationnelException() {
        super("Erreur rationnel, n <= 0 ou d = 0");
    }
}
