import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FileBorneeTest {

    private FileBornee fileBornee;

    @Before
    public void setUp() {
        fileBornee = new FileBornee(2);
    }

    @After
    public void tearDown() {
        fileBornee = null;
    }

    @Test
    public void p1() {
        assertTrue(fileBornee.isEmpty());
    }

    @Test
    public void p2() {
        int e = 1;
        fileBornee.enqueue(e);
        assertFalse(fileBornee.isEmpty());
    }

    @Test
    public void p3() {
        assertEquals(2, fileBornee.maxSize);
    }

    @Test
    public void p4() {
        int e = 1;
        fileBornee.enqueue(e);
        assertEquals(2, fileBornee.maxSize);
    }

    @Test
    public void p5() {
        assertEquals(0, fileBornee.getSize());
    }


    @Test
    public void p6A() {
        int e = 1;
        fileBornee.enqueue(e);
        fileBornee.enqueue(2);

        assertEquals(2, fileBornee.getSize());
    }

    @Test
    public void p6B() {
        int e = 1;
        fileBornee.enqueue(e);
        fileBornee.enqueue(e);
        fileBornee.enqueue(e);
        fileBornee.enqueue(e);
        fileBornee.enqueue(e);
        fileBornee.enqueue(e);

        assertEquals(2, fileBornee.maxSize);
    }

    @Test
    public void p7() {
        assertFalse(fileBornee.isFull());
    }

    @Test
    public void p8() {
        int e = 1;
        fileBornee.enqueue(e);
        fileBornee.enqueue(2);
        assertTrue(fileBornee.isFull());
    }

    @Test
    public void p9A() throws EmptyQueueException {
        int e = 1;
        fileBornee.enqueue(e);
        assertEquals(e, fileBornee.front());
    }

    @Test
    public void p9B() throws EmptyQueueException {
        int e = 1;
        fileBornee.enqueue(e);
        assertEquals(e, fileBornee.front());
        fileBornee.enqueue(2);
        assertEquals(e, fileBornee.front());
    }

    @Test
    public void p10A() throws EmptyQueueException {
        fileBornee.enqueue(1);
        fileBornee.dequeue();
        assertTrue(fileBornee.isEmpty());
    }

    @Test
    public void p10B() throws EmptyQueueException {
        int e1 = 1;
        int e2 = 2;
        fileBornee.enqueue(e1);
        fileBornee.enqueue(e2);
        fileBornee.dequeue();

        assertEquals(e2, fileBornee.front());
    }

    @Test(expected = IllegalArgumentException.class)
    public void precod1() {
        new FileBornee(-1);
    }

    @Test(expected = EmptyQueueException.class)
    public void precod2() throws EmptyQueueException {
        fileBornee.front();
    }

    @Test(expected = EmptyQueueException.class)
    public void precod3() throws EmptyQueueException {
        fileBornee.dequeue();
    }

}