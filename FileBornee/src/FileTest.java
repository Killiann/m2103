import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;

public class FileTest {

    private File<Integer> f;

    @Before
    public void setUp() {
        f = new File<Integer>();
    }

    @After
    public void tearDown() {
        f = null;
    }

    @Test
    public void testEstVideApresCreation() {
        assertTrue(this.f.isEmpty());

    }
    @Test
    public void testNestPlusVideApresAvoirEnfileUnElement() {
        this.f.enqueue(1);
        assertFalse(this.f.isEmpty());
    }

    @Test
    public void testEstVideApresCreationEnfilageEtConsommationDeTousLesElements() {

    }

    @Test
    public void testOnIncrementeBienLeNombreDElementsLorsquOnAjouteApresDernier() {

    }
    @Test
    public void testOnDecrementeBienLeNombreDElementsLorquOnConsommeLesPremiers() {

    }
    @Test
    public void testUneFileSeCaracteriseParFirstInFirstOut() {

    }
    @Test
    public void testOnNePeutConsommerUnElemenDuneFileVide() {

    }

}