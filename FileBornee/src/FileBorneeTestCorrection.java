import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FileBorneeTestCorrection {

    private FileBornee fileBornee;

    @Before
    public void setUp() {
        fileBornee = new FileBornee(2);
    }

    @After
    public void tearDown() {
        fileBornee = null;
    }

    @Test
    public void isEmpty() {
        assertTrue(fileBornee.isEmpty());
    }

    @Test
    public void isNotEmptyWhenAddElement() {
        fileBornee.enqueue(1);
        assertFalse(fileBornee.isEmpty());
    }

    @Test
    public void maxSizeIsConstant1() {
        assertEquals(2, fileBornee.maxSize);
    }

    @Test
    public void maxSizeIsConstant2() {
        int maxSize = fileBornee.maxSize;
        fileBornee.enqueue(1);
        assertEquals(maxSize, fileBornee.maxSize);
    }

    @Test
    public void sizeIsEmpty() {
        assertEquals(0, fileBornee.getSize());
    }

    @Test
    public void sizeverif() {
        fileBornee.enqueue(1);
        assertEquals(1, fileBornee.getSize());
        fileBornee.enqueue(2);
        assertEquals(2, fileBornee.getSize());
        fileBornee.enqueue(5);
        assertEquals(2, fileBornee.getSize());
    }

    @Test
    public void isFull() {
        assertFalse(fileBornee.isFull());
    }

    @Test
    public void checkFull() {
        fileBornee.enqueue(1);
        assertFalse(fileBornee.isFull());
        fileBornee.enqueue(1);
        assertTrue(fileBornee.isFull());
    }

    @Test
    public void checkFront() throws EmptyQueueException {
        fileBornee.enqueue(1);
        assertEquals(1, fileBornee.front());
        fileBornee.enqueue(2);
        assertEquals(1, fileBornee.front());
    }

    @Test
    public void checkFront2() throws EmptyQueueException {
        fileBornee.enqueue(1);
        assertEquals(1, fileBornee.front());
    }

    @Test
    public void checkDequeue() throws EmptyQueueException {
        fileBornee.enqueue(1);
        fileBornee.dequeue();
        assertTrue(fileBornee.isEmpty());
    }

    @Test
    public void checkDequeue2() throws EmptyQueueException {
        int e1 = 1;
        int e2 = 2;

        fileBornee.enqueue(e1);
        fileBornee.enqueue(e2);
        fileBornee.dequeue();

        FileBornee f1 = new FileBornee(2);
        f1.enqueue(e1);
        f1.dequeue();
        f1.enqueue(e2);

        assertEquals(fileBornee.front(), f1.front());
    }

    @Test
    public void checkDequeue3() throws EmptyQueueException {
        int e1 = 1;
        int e2 = 2;

        fileBornee.enqueue(e1);
        fileBornee.dequeue();
        fileBornee.enqueue(e2);

        FileBornee f1 = new FileBornee(2);
        f1.enqueue(e2);

        assertEquals(fileBornee.front(), f1.front());
    }


    @Test(expected = IllegalArgumentException.class)
    public void checkException1() throws IllegalArgumentException {
        new FileBornee(-1);
    }

    @Test(expected = EmptyQueueException.class)
    public void checkException2() throws EmptyQueueException {
        fileBornee.front();
    }

    @Test(expected = EmptyQueueException.class)
    public void checkException3() throws EmptyQueueException {
        fileBornee.dequeue();
    }

}