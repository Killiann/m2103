public class Point {

    public static Point ORIGINE = new Point(0, 0);

    protected int x, y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void translater(int tx, int ty) {
        this.x+=tx;
        this.y+=ty;
    }

    public void mettreAEchelle(int h) {
        if(h <= 0) throw new IllegalArgumentException();
        this.x*=h;
        this.y*=h;
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + ')';
    }
}
