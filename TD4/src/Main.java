import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Valeur de r:");
        int r = scanner.nextInt();
        System.out.println("Valeur de v:");
        int v = scanner.nextInt();
        System.out.println("Valeur de b:");
        int b = scanner.nextInt();

        PointCouleur pointCouleur = new PointCouleur(Point.ORIGINE, new Couleur(r, v, b));
        System.out.println(pointCouleur.toString());
        pointCouleur.couleurComplémentaire();
        System.out.println(pointCouleur.toString());
        scanner.close();
    }
}
