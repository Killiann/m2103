public class PointCouleur extends Point {

    private Couleur couleur;

    public PointCouleur(int x, int y, Couleur couleur) {
        super(x, y);
        this.couleur = couleur;
    }

    public PointCouleur(Point point, Couleur couleur) {
        super(point.getX(), point.getY());
        this.couleur = couleur;
    }

    public void couleurComplémentaire() {
        couleur.setR(255-couleur.getR());
        couleur.setG(255-couleur.getG());
        couleur.setB(255-couleur.getB());
    }

    public Couleur getCouleur() {
        return couleur;
    }

    @Override
    public String toString() {
        return super.toString() + couleur.toString();
    }
}
