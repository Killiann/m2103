import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.NoSuchElementException;

import static org.junit.Assert.*;

public class FileTest {

    private File<Integer> file;

    @Before
    public void setUp() {
        this.file = new File<Integer>();
    }

    @After
    public void tearDown() {
        this.file = null;
    }

    @Test
    public void testEstVideApresCreation() {
        assertTrue(this.file.estVide());
    }

    @Test
    public void testNestPlusVideApresAvoirEnfileUnElement() {
        this.file.enfiler(1);
        assertFalse(this.file.estVide());
    }

    @Test
    public void testEstVideApresCreationEnfilageEtConsommationDeTousLesElements() {
        this.file.enfiler(1);
        this.file.défiler();
        assertTrue(this.file.estVide());
    }

    @Test
    public void testOnIncrementeBienLeNombreDElementsLorsquOnAjouteApresDernier() {
        assertEquals(0, this.file.nbElements());
        this.file.enfiler(1);
        assertEquals(1, this.file.nbElements());
        this.file.enfiler(5);
        assertEquals(2, this.file.nbElements());
    }

    @Test
    public void testOnDecrementeBienLeNombreDElementsLorquOnConsommeLesPremiers() {
        this.file.enfiler(1);
        assertEquals(1, this.file.nbElements());
        this.file.défiler();
        assertEquals(0, this.file.nbElements());
    }

    @Test
    public void testUneFileSeCaracteriseParFirstInFirstOut() {
        this.file.enfiler(2);
        this.file.enfiler(1);
        assertEquals(2, (int)this.file.défiler());
        assertEquals(1, (int)this.file.défiler());
    }

    @Test(expected = NoSuchElementException.class) // pas de throws comme NoSuchElementException extends RuntimeException
    public void testOnNePeutConsommerUnElemenDuneFileVide() {
        this.file.défiler();
    }

}