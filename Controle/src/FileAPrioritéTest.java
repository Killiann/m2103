import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.NoSuchElementException;

import static org.junit.Assert.*;

public class FileAPrioritéTest {

    private FileAPriorité fileAPriorité;

    @Before
    public void setUp() {
        this.fileAPriorité = new FileAPriorité(3);
    }

    @After
    public void tearDown() {
        this.fileAPriorité = null;
    }

    @Test(expected = IllegalArgumentException.class) // pas de throws comme IllegalArgumentException extends RuntimeException
    public void testFileAvecUnNombreNegatifDePriorités() {
        new FileAPriorité(-1);
    }

    @Test(expected = IllegalArgumentException.class) // pas de throws comme IllegalArgumentException extends RuntimeException
    public void testFileAvecUnNombreAZeroDePriorités() {
        new FileAPriorité(0);
    }

    @Test(expected = IllegalArgumentException.class) // pas de throws comme IllegalArgumentException extends RuntimeException
    public void testFileAvecUnNombreDePrioritésExcessifs() {
        new FileAPriorité(100);
    }

    @Test
    public void testLesFilesAPrioritéSontNonNulles() {
        assertNotNull(this.fileAPriorité.getFileDeJobsDePriorité(0));
        assertNotNull(this.fileAPriorité.getFileDeJobsDePriorité(1));
        assertNotNull(this.fileAPriorité.getFileDeJobsDePriorité(2));
        /* ou assertTrue(this.fileAPriorité.getFileDeJobsDePriorité(0)!=null);
        assertTrue(this.fileAPriorité.getFileDeJobsDePriorité(1)!=null);
        assertTrue(this.fileAPriorité.getFileDeJobsDePriorité(é)!=null);
         */
    }

    @Test
    public void testEtLesFilesAPrioritéSontVides() {
        assertTrue(this.fileAPriorité.getFileDeJobsDePriorité(0).estVide());
        assertTrue(this.fileAPriorité.getFileDeJobsDePriorité(1).estVide());
        assertTrue(this.fileAPriorité.getFileDeJobsDePriorité(2).estVide());
    }

    @Test(expected = IllegalArgumentException.class) // pas de throws comme IllegalArgumentException extends RuntimeException
    public void testAjoutDunJobPrioritéInférieurOuEgalAZero () {
        this.fileAPriorité.ajouterUnJobAPrioriser(new Job(-1, "TestJob", 2000));
    }

    @Test(expected = IllegalArgumentException.class) // pas de throws comme IllegalArgumentException extends RuntimeException
    public void testAjoutDunJobPrioritéSupérieurACapacitéDeLaFileAPriorité () {
        this.fileAPriorité.ajouterUnJobAPrioriser(new Job(100, "TestJob", 2000));
    }

    @Test
    public void testAJoutDeJobsALaBonnePriorité() {
        this.fileAPriorité.ajouterUnJobAPrioriser(new Job(1, "TestJob", 2000));
        assertFalse(this.fileAPriorité.getFileDeJobsDePriorité(0).estVide());

        this.fileAPriorité.ajouterUnJobAPrioriser(new Job(2, "TestJob2", 4000));
        assertFalse(this.fileAPriorité.getFileDeJobsDePriorité(1).estVide());
    }

    @Test
    public void testFileAPrioritéGlobalementVideApresCreation() {
        assertTrue(this.fileAPriorité.estVide());
    }

    @Test
    public void testFileAPrioritéGlobalementNonVideApresCreationEtAjoutDunJob() {
        this.fileAPriorité.ajouterUnJobAPrioriser(new Job(1, "TestJob", 2000));
        assertFalse(this.fileAPriorité.estVide());
    }

    @Test(expected = NoSuchElementException.class) // pas de throws comme NoSuchElementException extends RuntimeException
    public void testConsommationDunJobSurUneFileAPrioriteGlobalementVide() {
        assertTrue(this.fileAPriorité.estVide());
        this.fileAPriorité.consommerPremierJobPlusGrandePriorité();
    }

    @Test
    public void testConsommationDeJobsParPrioritéDécroissante() {
        Job job1, job2, job3, job4;
        this.fileAPriorité.ajouterUnJobAPrioriser(job4 = new Job(3, "TestJob2", 1000));
        this.fileAPriorité.ajouterUnJobAPrioriser(job1 = new Job(1, "TestJob", 2000));
        this.fileAPriorité.ajouterUnJobAPrioriser(job2 = new Job(1, "TestJob12", 2000));
        this.fileAPriorité.ajouterUnJobAPrioriser(job3 = new Job(2, "TestJob2", 4000));

        assertEquals(job4, fileAPriorité.consommerPremierJobPlusGrandePriorité());
        assertEquals(job3, fileAPriorité.consommerPremierJobPlusGrandePriorité());
        assertEquals(job1, fileAPriorité.consommerPremierJobPlusGrandePriorité());
        assertEquals(job2, fileAPriorité.consommerPremierJobPlusGrandePriorité());
    }

    @Test
    public void testConsommationDeTousLesJobsAvecTempsDexecution() {
        this.fileAPriorité.ajouterUnJobAPrioriser(new Job(1, "TestJob", 2000));
        this.fileAPriorité.ajouterUnJobAPrioriser(new Job(2, "TestJob2", 4000));
        this.fileAPriorité.ajouterUnJobAPrioriser(new Job(3, "TestJob2", 1000));

        assertEquals(7000, this.fileAPriorité.consommerLesJobsParPrioritéDécroissante());
    }

}