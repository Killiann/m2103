public class Compte {
    private float credit;
    private float debit;

    public Compte() {
        this.credit = 0F;
        this.debit = 0F;
    }

    public float getSolde() {
        return this.credit - this.debit;
    }

    public void deposer(float montant) throws IllegalArgumentException  {
        if (montant < 0)
            throw new IllegalArgumentException("Montant positif exigé");
        this.credit+=montant;
    }

    public void retirer(float montant) throws IllegalArgumentException  {
        if (montant < 0)
            throw new IllegalArgumentException();
        this.debit+=montant;
    }

    @Override
    public String toString() {
        return "Crédit : " + this.credit + ", Débit : " + this.debit;
    }

}
