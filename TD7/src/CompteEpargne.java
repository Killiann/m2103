public class CompteEpargne extends CompteBancaire {
    private float tauxEnPourcentage;

    public CompteEpargne(String numero, float tauxEnPourcentage) throws IllegalArgumentException {
        super(numero);
        if (tauxEnPourcentage <= 0)
            throw new IllegalArgumentException("Le taux doit etre strictement positif");
        this.tauxEnPourcentage = tauxEnPourcentage;
    }

    public float getTauxEnPourcentage() {
        return this.getTauxEnPourcentage();
    }

    public float interets() {
        if (this.getSolde() > 0)
            return this.getSolde() * this.tauxEnPourcentage /100;
        else
            return 0F;
    }

    public void ajouterInterets() {
        this.deposer(this.interets());
    }

    @Override
    public String toString() {
        return super.toString() + ", Taux : " + this.tauxEnPourcentage + "%";
    }

}

