public class CompteBancaire extends Compte {

    private String numero;

    public CompteBancaire(String numero) {
        super();
        this.numero = numero;
    }

    public String getNumero() {
        return this.numero;
    }

    @Override
    public String toString() {
        return "Numéro : " + this.numero + ", " + super.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        assert(obj != null);
        assert(obj instanceof CompteBancaire);
        //Je peut commencer a travailler sur l'egalite de 2 comptes bancaires
        CompteBancaire other = (CompteBancaire) obj;
        // cas du compte qui n'a pas de numeros
        if (numero == null) {
            if (other.numero != null)
                return false;
        }
        //une seul ligne pour decrire l'egalite semantique entre 2 comptes bancaires
        return(this.numero.equals(other.numero));
    }



}
