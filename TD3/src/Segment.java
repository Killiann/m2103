public class Segment {

    private Point orgine;
    private Point extreminte;

    public Segment(Point orgine, Point extreminte) {
        this.orgine = orgine;
        this.extreminte = extreminte;
    }

    public Point pointMilieu() {
        return new Point((orgine.getX()+extreminte.getX())/2, (orgine.getY()+extreminte.getY())/2);
    }
}
