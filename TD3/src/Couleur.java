public class Couleur {

    public static Couleur ROUGE = new Couleur(255, 0, 0);
    public static Couleur VERT = new Couleur(0, 255, 0);
    public static Couleur BLEU = new Couleur(0, 0, 255);

    private int r, g, b;

    public Couleur(int r, int g, int b) throws IllegalArgumentException {
        if((r < 0 || r > 255) || (g < 0 || g > 255) || (b < 0 || b > 255)) throw new IllegalArgumentException();
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public int getR() {
        return r;
    }

    public int getG() {
        return g;
    }

    public int getB() {
        return b;
    }

    public int valeurRVB() {
        return this.r*65536+g*256+b;
    }

    public void setR(int r) throws IllegalArgumentException {
        if(r < 0 || r > 255) throw new IllegalArgumentException();
        this.r = r;
    }

    public void setG(int g) throws IllegalArgumentException {
        if(g < 0 || g > 255) throw new IllegalArgumentException();
        this.g = g;
    }

    public void setB(int b) {
        if(b < 0 || b > 255) throw new IllegalArgumentException();
        this.b = b;
    }

    @Override
    public String toString() {
        return "[" + r + "," + g + "," + b + ']';
    }
}
