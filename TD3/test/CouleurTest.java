import org.junit.Test;
import org.junit.Before; 
import org.junit.After;

import static org.junit.Assert.assertEquals;

public class CouleurTest {

    private Couleur c;
    @Before
    public void setUp() throws Exception {
        this.c = new Couleur(1,2,3);
    }
    @After
    public void tearDown() throws Exception {
        this.c = null;
    }

    @Test
    public void testComposanteRougeConstructeur() {
        assertEquals(1,this.c.getR());
    }
    @Test
    public void testComposanteVerteConstructeur() {
        assertEquals(2,this.c.getG());
    }
    @Test
    public void testComposanteBleuConstructeur() {
        assertEquals(3,this.c.getB());
    }
    @Test
    public void testComposanteRougeSetter() {
        this.c.setR(255);
        assertEquals(255,this.c.getR());
    }
    @Test
    public void testComposanteVerteSetter() {
        this.c.setG(255);
        assertEquals(255,this.c.getG());
    }
    @Test
    public void testComposanteBleuSetter() {
        this.c.setB(255);
        assertEquals(255,this.c.getB());
    }
    @Test
    public void testConstanteRouge() {
        assertEquals(255,Couleur.ROUGE.getR());
        assertEquals(0,Couleur.ROUGE.getG());
        assertEquals(0,Couleur.ROUGE.getB());
    }
    @Test
    public void testConstanteVert() {
        assertEquals(0,Couleur.VERT.getR());
        assertEquals(255,Couleur.VERT.getG());
        assertEquals(0,Couleur.VERT.getB());
    }
    @Test
    public void testConstanteBleu() {
        assertEquals(0,Couleur.BLEU.getR());
        assertEquals(0,Couleur.BLEU.getG());
        assertEquals(255,Couleur.BLEU.getB());
    }
} 
