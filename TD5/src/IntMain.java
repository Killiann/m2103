public class IntMain {

    public static void main(String[] args) {
        Intervalle i1 = new Intervalle(4, 20);
        Intervalle i2 = new Intervalle(-6, 9);

        if(!i1.disjoint(i2)) {
            Intervalle i3 = i1.inter(i2);
            System.out.println(i3);

            int[] tab = new int[i3.getBorneSup()-i3.getBorneInf()+1];
            for (int x = i3.getBorneInf(); x <= i3.getBorneSup(); x++) {
                tab[x-i3.getBorneInf()] = 10*x+5;
            }

            for (int i : tab) {
                System.out.print(i + "\t");
            }

            System.out.println();
            for (int i = 0; i < tab.length; i++) {
                System.out.print("f(" + (i+i3.getBorneInf()) + ") = " + tab[i]);
                System.out.println();
            }

        } else {
            System.out.println("Les intervalles sont disjoit");
        }
    }
}
