import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        new Main();
    }

    public Main() {
        Menu menu = new Menu();
        menu.registerPage(new Page<Monome>(1, "modifier le 1er monome", (monome, monome2) -> update(true, menu)));
        menu.registerPage(new Page<Monome>(2, "modifier le 2eme monome", (monome, monome2) -> update(false, menu)));
        menu.registerPage(new Page<Monome>(3, "afficher le premier monôme", (monome, monome2) -> System.out.println("1er monome: " + monome.toString())));
        menu.registerPage(new Page<Monome>(4, "afficher le deuxième monôme", (monome, monome2) -> System.out.println("2eme monome: " + monome2.toString())));
        menu.registerPage(new Page<Monome>(5, "calculer la somme des 2 monômes", (monome, monome2) -> System.out.println("Somme des 2 monomes: " + monome.somme(monome2).toString())));
        menu.registerPage(new Page<Monome>(6, "calculer le produit des 2 monômes", (monome, monome2) -> System.out.println("Produit des 2 monomes: " + monome.produit(monome2).toString())));
        menu.registerPage(new Page<Monome>(7, "calculer la dérivée du premier monôme", (monome, monome2) -> System.out.println("Dérivée du monome1: " + monome.dérivée().toString())));
        menu.registerPage(new Page<Monome>(8, "calculer la dérivée du deuxieme monôme", (monome, monome2) -> System.out.println("Dérivée du monome2: " + monome2.dérivée().toString())));
        menu.registerPage(new Page<Monome>(9, "quitter l'application", (monome, monome2) -> menu.close()));

        menu.display();
    }

    private void update(boolean first, Menu menu) {
        float coef;
        int exp;
        System.out.println("Coef ?");
        Scanner scanner = menu.getScanner();
        coef = scanner.nextFloat();
        System.out.println("Exp ?");
        exp = scanner.nextInt();
        menu.setMonome(first, new Monome(coef, exp));
    }
}
