import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class Page<T> {

    private final int id;
    private final String name;
    private final BiConsumer<T, T> consumer;

    public Page(int id, String name, BiConsumer<T, T> consumer) {
        this.id = id;
        this.name = name;
        this.consumer = consumer;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public BiConsumer<T, T> getConsumer() {
        return consumer;
    }

    @Override
    public String toString() {
        return "    "+id+ "- " + name;
    }
}
