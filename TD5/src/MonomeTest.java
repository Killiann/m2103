import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

/**
 * Monome Tester.
 *
 * @author Killian Falguiere
 * @version 1.0
 */
public class MonomeTest {

    @Test
    public void testSomme() {
        Monome monome1 = new Monome(1F, 0).somme(new Monome(1F, 0));
        Assert.assertEquals(monome1.getCoef(), 2F, 1F);
        Assert.assertEquals(monome1.getExp(), 0);
    }

    @Test
    public void testProduit() {
        Monome monome1 = new Monome(1F, 0).produit(new Monome(1F, 0));
        Assert.assertEquals(monome1.getCoef(), 1F, 1F);
        Assert.assertEquals(monome1.getExp(), 0);
    }

    @Test
    public void testDérivée() {
        Monome monome1 = new Monome(1F, 1).dérivée();
        Assert.assertEquals(monome1.getCoef(), 1F, 1F);
        Assert.assertEquals(monome1.getExp(), 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testError() {
        new Monome(1F, -1);
    }

    @Test
    public void testEstNul() {
        Assert.assertTrue(new Monome(0F, 0).estNul());
    }

    @Test
    public void testGetCoef() {
        Assert.assertEquals(new Monome(0F, 0).getCoef(), 0F, 1F);
    }

    @Test
    public void testGetExp() {
        Assert.assertEquals(new Monome(0F, 0).getExp(), 0);
    }
} 
