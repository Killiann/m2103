import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Menu {

    private List<Page<?>> pages = new ArrayList<>();

    private Monome monome1 = new Monome(0F, 0);
    private Monome monome2 = new Monome(0F, 0);

    private Scanner scanner;

    public void registerPage(Page<?> page) {
        pages.add(page);
    }

    public void display() {
        scanner = new Scanner(System.in);

        while (true) {
            System.out.println(toString());
            int id = scanner.nextInt();
            Page<Object> page = (Page<Object>) pages.stream().filter(page1 -> page1.getId()==id).findFirst().orElse(null);
            if(page == null) continue;
            page.getConsumer().accept(monome1, monome2);
        }
    }

    public void close() {
        scanner.close();
        System.exit(501);
    }

    public void setMonome(boolean first, Monome monome1) {
        if(first) {
            this.monome1 = monome1;
        } else {
            this.monome2 = monome1;
        }
    }

    public Scanner getScanner() {
        return scanner;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Quel est votre choix :\n");
        pages.forEach(page -> builder.append(page.toString()).append("\n"));
        return builder.toString();
    }
}
