public class Intervalle {
    private final int borneInf;
    private final int borneSup;

    public static final Intervalle INFINI_ZERO = new Intervalle(Integer.MIN_VALUE, 0);
    public static final  Intervalle ZERO_INFINI = new Intervalle(0, Integer.MAX_VALUE);

    public Intervalle(int borneInf, int borneSup) throws IllegalArgumentException {
        if (borneInf >= borneSup) {
            throw new IllegalArgumentException("pas bon ");
        }
        this.borneInf = borneInf;
        this.borneSup = borneSup;

    }

    public int getBorneInf() {
        return borneInf;
    }

    public int getBorneSup() {
        return borneSup;
    }

    public boolean comprend(int entier) {
        if (entier >= this.borneInf && entier <= this.borneSup) {
            return true;
        }
        return false;
    }

    public boolean disjoint(Intervalle another) {
        if (this.borneInf > another.borneSup || this.borneSup < another.borneInf) {
            return true;
        }
        return false;
    }

    public int max(int v1, int v2) {
        if (v1 < v2) {
            return v2;
        }
        return v1;

    }

    public int min(int v1, int v2) {
        if (v1 < v2) {
            return v1;
        }
        return v2;

    }

    public Intervalle union(Intervalle another) throws IllegalArgumentException {
        if (this.disjoint(another)) {
            throw new IllegalArgumentException("pas bon");
        }
        return new Intervalle(min(this.borneInf, another.borneInf), max(this.borneSup, another.borneSup));

    }

    public Intervalle inter(Intervalle another) throws IllegalArgumentException {
        if (this.disjoint(another)) {
            throw new IllegalArgumentException("pas bon");
        }
        return new Intervalle(max(this.borneInf, another.borneInf), min(this.borneSup, another.borneSup));

    }

    @Override
    public String toString() {

        return "[" + this.borneInf + " ; " + this.borneSup + "]";
    }
}