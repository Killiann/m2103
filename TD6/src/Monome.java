/**
 * Classe définissant un monome
 *
 * @author Killian Falguiere
 * @version 1
 */
public class Monome {

    private float coef;
    private int exp;

    /**
     * Permets de construire un monome
     *
     * @param f
     *          coefficient d'un monome
     * @param m
     *          exposant d'un monome
     * @throws IllegalArgumentException
     *                                  si l'exposant est négatif
     */
    public Monome(float f, int m) throws IllegalArgumentException {
        if(m < 0) throw new IllegalArgumentException();
        this.coef = f;
        this.exp = m;
    }

    /**
     * Additioner 2 monomes
     *
     * @param monome
     *              Le monome à additioner
     * @return
     *          la somme des 2 monomes
     * @throws ArithmeticException
     *                              si les exposants ne sont pas égaux
     */
    public Monome somme(Monome monome) throws ArithmeticException {
        if(this.exp != monome.getExp()) throw new ArithmeticException();
        return new Monome(this.coef+monome.getCoef(), this.exp);
    }

    /**
     * Calcule le produit de 2 monomes
     *
     * @param monome
     *              Le monome à multiplier
     * @return
     *          le produit des 2 monomes
     */
    public Monome produit(Monome monome) {
        return new Monome(this.coef*monome.getCoef(), this.exp+monome.getExp());
    }

    /**
     * Calcule la dérivée d'un monome
     *
     * @return
     *          la dérivée d'un monome
     */
    public Monome dérivée() {
        if(this.exp == 0) return new Monome(0F, 0);
        return new Monome(this.exp*this.coef, this.exp-1);
    }

    /**
     * Observateur
     *
     * @return vraie quand le coef est nul, soit = 0
     */
    public boolean estNul() {
        return this.coef == 0F;
    }

    /**
     * Observateur
     *
     * @return coef d'un monome
     */
    public float getCoef() {
        return this.coef;
    }

    /**
     * Observateur
     *
     * @return exposant d'un monome
     */
    public int getExp() {
        return this.exp;
    }

    /**
     * Surcharger de la méthode toString de la class Object
     *
     * @return
     *          un monome sous la forme coefxeexposant
     */
    @Override
    public String toString() {
        return this.coef+ (this.exp >= 1 ? "x" : "") + (this.exp == 0 ? "" : "e"+this.exp);
    }
}
