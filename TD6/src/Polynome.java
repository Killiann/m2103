/**
 * Class polunome
 *
 * @author Killian
 * @version 1
 *
 */
public class Polynome {

    private Monome monome;

    /**
     * Construit un polunome null
     */
    public Polynome() {}

    /**
     * Permets de définir le monome du polynome
     *
     * @param monome
     */
    public void setMonome(Monome monome) {
        this.monome = monome;
    }

    /**
     * Permets de récuperrer le monome
     *
     * @param e
     * @return
     * @throws IllegalArgumentException
     */
    public Monome getMonome(int e) throws IllegalArgumentException {
        if(e < 0)
            throw new IllegalArgumentException();
        return monome == null ? new Monome(0, e) : (monome.getExp() == e ? monome : getMonome(e));
    }

    /**
     * Permets de faire la somme entre ce polynome et un autre polynome
     *
     * @param polynome
     *                  polunome utilisé pour faire la somme
     * @return
     *          polynome résultat de la somme
     */
    public Polynome somme(Polynome polynome) {
        return null;
    }

    /**
     * Permets de faire le produit entre ce polynome et un autre polynome
     *
     * @param monome
     *              polynome utilisé pour faire le produit
     * @return
     *          polynome résultat du produit
     */
    public Polynome produit(Monome monome) {
        return null;
    }

    /**
     * Permets d'avoir la dérivée du monome
     *
     * @return
     *          dérivé de ce polynome
     */
    public Polynome derive() {
        return null;
    }
}
