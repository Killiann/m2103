package basic;

public class Rationnel {

    private final long n;
    private final long d;

    public Rationnel(long n, long d) throws IllegalArgumentException {
        if(d == 0L) throw new IllegalArgumentException();
        this.n = n;
        this.d = d;
    }

    public long getNumérateur() {
        return n;
    }

    public long getDénominateur() {
        return d;
    }

    public Rationnel réduction() {
        return new Rationnel(n/pgcd(n, d), d/pgcd(n, d));
    }

    public Rationnel somme(Rationnel r) {
        return new Rationnel(n*r.d + r.n*d, d*r.d);
    }

    public Rationnel produit(Rationnel r) {
        return new Rationnel(n*r.getNumérateur(), d*r.getDénominateur());
    }

    public Rationnel division(Rationnel r) {
        return new Rationnel(n*r.d, d*r.n);
    }

    public static long pgcd(long n, long d) {
        if(n > d) {
            return pgcdd(n - d, d);
        } else if(d > n) {
            return pgcdd(n, d - n);
        }
        return pgcdd(n, d);
    }

    private static long pgcdd(long a, long b) {
        long pgcd = 0;
        for(int i=1; i <= a && i <= b; i++) {
            if(a% i==0 && b%i==0)
                pgcd = i;
        }
        return pgcd;
    }

    @Override
    public String toString() {
        return n+"/"+d;
    }
}
