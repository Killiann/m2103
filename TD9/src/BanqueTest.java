import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

public class BanqueTest {

    @Test
    public void testOuvrir() {
        new Banque("0").ouvrir("0", 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOuvrirException1() {
        Banque banque = new Banque("0");
        banque.ouvrir("0", 1);
        banque.ouvrir("0", 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOuvrirException2() {
        new Banque("0").ouvrir("1", -1);
    }

    @Test
    public void testFermer() {
        Banque banque = new Banque("0");
        banque.ouvrir("0", 0);
        banque.fermer("0");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFermerException1() {
        new Banque("0").fermer("0");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFermerException2() {
        Banque banque = new Banque("0");
        banque.ouvrir("0", 1);
        banque.fermer("0");
    }

    @Test
    public void testSolde() throws Exception {
        Banque banque = new Banque("0");
        banque.ouvrir("0", 1);
        banque.ouvrir("1", 10);
        Assert.assertEquals(banque.solde(), 11F, 1F);
    }

    @Test
    public void testDéposer() {
        Banque banque = new Banque("0");
        banque.ouvrir("0", 1);
        banque.déposer("0", 10F);
        Assert.assertEquals(banque.getCompte("0").solde(), 11F, 1F);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDéposerException1() {
        Banque banque = new Banque("0");
        banque.déposer("0", 10F);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDéposerException2() {
        Banque banque = new Banque("0");
        banque.ouvrir("0", 1);
        banque.déposer("0", -10F);
    }

    @Test
    public void testRetirer() {
        Banque banque = new Banque("0");
        banque.ouvrir("0", 1);
        banque.retirer("0", 1F);
        Assert.assertEquals(banque.getCompte("0").solde(), 0F, 1F);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testtestRetirerException1() {
        Banque banque = new Banque("0");
        banque.retirer("0", 10F);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testtestRetirerException2() {
        Banque banque = new Banque("0");
        banque.ouvrir("0", 1);
        banque.retirer("0", -10F);
    }

    @Test
    public void testExiste() {
        Banque banque = new Banque("0");
        banque.ouvrir("0", 1);
        Assert.assertTrue(banque.existe("0"));
    }

    @Test
    public void testGetCompte() {
        Banque banque = new Banque("0");
        banque.ouvrir("0", 1);
        Assert.assertTrue(banque.getCompte("0")!=null);
    }

    @Test
    public void testCompte() {
        Banque banque = new Banque("0");
        banque.ouvrir("0", 1);
        Assert.assertTrue(banque.compte("0")!=null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCompteException() {
        Banque banque = new Banque("0");
        banque.compte("0");
    }

    @Test
    public void testGetLibellé() {
        Banque banque = new Banque("0");
        Assert.assertEquals(banque.getLibellé(), "0");
    }

    @Test
    public void testToString() {
        Banque banque = new Banque("Toto");
        banque.ouvrir("001", 15F);
        banque.ouvrir("002", 10F);

        Assert.assertEquals(banque.toString(), "[Banque : Toto" +
                "\nNuméro : 001, Crédit : 15.0, Débit : 0.0" +
                "\nNuméro : 002, Crédit : 10.0, Débit : 0.0]");
    }
} 
