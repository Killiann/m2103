public class CompteEpargne extends CompteBancaire {
    /**
     * taux d'intéréts
     */
    private float taux;

    /**
     * crée un compte épargne vide
     * 
     * @param numéro
     *            numéro du compte épargne
     * @param taux
     *            taux d'intéréts
     * @param IllegalArgumentException
     *            si t <= 0
     */
    public CompteEpargne(String numéro, float t)
            throws IllegalArgumentException {
        super(numéro);
        if (t <= 0) {
            throw new IllegalArgumentException("taux d'intéréts négatif ou nul");
        }
        this.taux = t;
    }

    /**
     * fournit le taux d'intéréts d'un compte épargne
     * 
     * @return taux d'intéréts
     */
    public float getTaux() {
        return this.taux;
    }

    /**
     * calcule les intéréts d'un compte épargne
     * 
     * @return intéréts
     */
    public float intéréts() {
        if (this.solde() > 0) {
            return this.solde() * this.getTaux();
        } else {
            return 0.0F;
        }
    }

    /**
     * ajoute é un compte épargne ses intéréts
     */
    public void ajouterIntéréts() {
        this.déposer(this.intéréts());
    }

    /**
     * fournit une version unicode d'un compte épargne
     * 
     * @return chaéne contenant le numéro concaténé au crédit concaténé au débit
     *         concaténé au taux d'intéréts
     */
    @Override
    public String toString() {
        return super.toString() + ", Taux : " + this.getTaux() * 100 + "%";
    }

}
