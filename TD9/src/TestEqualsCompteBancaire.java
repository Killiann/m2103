import org.junit.Test;

import static org.junit.Assert.*;

public class TestEqualsCompteBancaire {

    @Test
    public void testRéfléxivité() {
        CompteBancaire cb = new CompteBancaire("0940087777");
        assertTrue(cb.equals(cb));
    }

    @Test
    public void testSame() {
        CompteBancaire cb = new CompteBancaire("0940087777");
        assertSame(cb,cb);
        // m�me chose
        assertTrue(cb == cb);
    }

    @Test
    public void testParamétreNul() {
        CompteBancaire cb = new CompteBancaire("0940087777");
        assertFalse(cb.equals(null));
    }

    @Test(expected = NullPointerException.class)
    public void testObjetNul() {
        CompteBancaire cbNull = null;
        CompteBancaire cb = new CompteBancaire("0940087777");
        assertFalse(cbNull.equals(cb));
    }

    @Test
    public void testEquals2TypesDifférents() {
        CompteBancaire cb = new CompteBancaire("0940087777");
        assertFalse(cb.equals(new String("0940087777")));
    }

    @Test
    public void testEqualsCompteNumeroNulAvecCompteNumeroNonNul() {
        CompteBancaire cb = new CompteBancaire(null);
        CompteBancaire another = new CompteBancaire("0940087777");
        assertFalse(cb.equals(another));
    }

    @Test
    public void testEqualsCompteNumeroNonNulAvecCompteNumeroNul() {
        CompteBancaire cb = new CompteBancaire(null);
        CompteBancaire another = new CompteBancaire("0940087777");
        assertFalse(another.equals(cb));
    }

    @Test
    public void testEqualsCompteNumerosIdentiques() {
        CompteBancaire cb = new CompteBancaire("0940087777");
        CompteBancaire another = new CompteBancaire("0940087777");
        assertTrue(cb.equals(another));
    }

    @Test
    public void testEqualsCompteNumerosNonIdentiques() {
        CompteBancaire cb = new CompteBancaire("0940087777");
        CompteBancaire another = new CompteBancaire("0940097777");
        assertFalse(cb.equals(another));
    }
}
