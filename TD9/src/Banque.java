import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class Banque {

    private String libellé;

    private List<CompteBancaire> compteBancaires = new ArrayList<>();

    public Banque(String libellé) {
        this.libellé = libellé;
    }

    public void ouvrir(String numéro, float val) throws IllegalArgumentException {
        if(existe(numéro)) throw new IllegalArgumentException("Compte existe déjà");
        if(val < 0) throw new IllegalArgumentException("val < 0");
        CompteBancaire compteBancaire = new CompteBancaire(numéro);
        compteBancaire.déposer(val);
        compteBancaires.add(compteBancaire);
    }

    public void fermer(String numéro) throws IllegalArgumentException {
        if(!existe(numéro)) throw new IllegalArgumentException("Compte n'existe pas");
        CompteBancaire compteBancaire = compte(numéro);
        if(compteBancaire.solde() != 0) throw new IllegalArgumentException("solde != 0");
        compteBancaires.remove(compteBancaire);
    }

    public float solde() {
        float val = 0F;
        for (CompteBancaire compteBancaire : compteBancaires) {
            val+=compteBancaire.solde();
        }
        return val;
    }

    public void déposer(String numéro, float val) throws IllegalArgumentException{
        if(!existe(numéro)) throw new IllegalArgumentException("Compte n'existe pas");
        if(val < 0) throw new IllegalArgumentException("val < 0");
        CompteBancaire compteBancaire = compte(numéro);
        compteBancaire.déposer(val);
    }

    public void retirer(String numéro, float val) throws IllegalArgumentException {
        if(!existe(numéro)) throw new IllegalArgumentException("Compte n'existe pas");
        if(val < 0) throw new IllegalArgumentException("val < 0");
        CompteBancaire compteBancaire = compte(numéro);
        compteBancaire.retirer(val);
    }

    public boolean existe(String numéro) {
        return getCompte(numéro) != null;
    }

    public CompteBancaire getCompte(String numéro) {
        return compteBancaires.stream().filter(c -> c.getNuméro().equals(numéro)).findFirst().orElse(null);
    }

    public CompteBancaire compte(String numéro) throws IllegalArgumentException {
        if(!existe(numéro)) throw new IllegalArgumentException("Compte n'existe pas");
        return compteBancaires.stream().filter(c -> c.getNuméro().equals(numéro)).findFirst().orElse(null);
    }

    public String getLibellé() {
        return libellé;
    }

    public void affichercomptesDébiteurs() {
        compteBancaires.stream().filter(compteBancaire -> compteBancaire.solde() < 0).forEach(compteBancaire -> System.out.println(compteBancaire.toString()));
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("[Banque : " + libellé);
        compteBancaires.forEach(compteBancaire -> stringBuilder.append("\n").append(compteBancaire.toString()));
        return stringBuilder.append("]").toString();
    }
}
